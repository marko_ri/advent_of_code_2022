struct Input {
    stacks: Vec<Vec<u8>>,
    instructions: Vec<Vec<u8>>,
}

fn parse_input(input: &str) -> Input {
    let mut parts = input.split("\n\n");
    let upper = parts.next().unwrap();
    let lower = parts.next().unwrap();

    let mut stacks: Vec<Vec<u8>> = vec![vec![]; 9];
    for (i, c) in upper.lines().flat_map(|l| l.char_indices()) {
        if i > 0 && (i - 1) % 4 == 0 && !c.is_ascii_whitespace() && !c.is_ascii_digit() {
            stacks[i / 4].push(c as u8);
        }
    }

    for stack in stacks.iter_mut() {
        stack.reverse();
    }

    let mut instructions: Vec<Vec<u8>> = vec![];
    for line in lower.lines() {
        let mut instruction = vec![];
        for (i, s) in line.split_ascii_whitespace().enumerate() {
            if i == 1 || i == 3 || i == 5 {
                instruction.push(s.parse().expect("number"));
            }
        }
        instructions.push(instruction);
    }

    Input {
        stacks,
        instructions,
    }
}

fn part1(input: &Input) -> String {
    let mut stacks = input.stacks.clone();
    for instruction in input.instructions.iter() {
        let amount = instruction[0];
        let from = instruction[1] - 1;
        let to = instruction[2] - 1;

        for _ in 0..amount {
            if let Some(pop) = stacks[from as usize].pop() {
                stacks[to as usize].push(pop);
            }
        }
    }

    stacks
        .iter()
        .filter_map(|stack| stack.iter().last())
        .map(|b| char::from(*b))
        .collect()
}

fn part2(mut input: Input) -> String {
    for instruction in input.instructions.iter() {
        let mov = instruction[0];
        let from = instruction[1] - 1;
        let to = instruction[2] - 1;

        let split_at = input.stacks[from as usize].len() - mov as usize;
        let pop = input.stacks[from as usize].split_off(split_at);
        input.stacks[to as usize].extend(pop);
    }

    input
        .stacks
        .iter()
        .filter_map(|stack| stack.iter().last())
        .map(|b| char::from(*b))
        .collect()
}

fn main() {
    let input = parse_input(include_str!("../input/day05.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(input);
    println!("part2: {:?}", part2);
}
