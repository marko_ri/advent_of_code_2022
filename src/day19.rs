use once_cell::sync::Lazy;
use regex::Regex;
use std::collections::VecDeque;
use std::str::FromStr;

const IND_ORE: usize = 0;
const IND_CLY: usize = 1;
const IND_OBS: usize = 2;
const IND_GEO: usize = 3;

static REGEX: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        "Blueprint (\\d+): \
       Each ore robot costs (\\d+) ore. \
       Each clay robot costs (\\d+) ore. \
       Each obsidian robot costs (\\d+) ore and (\\d+) clay. \
       Each geode robot costs (\\d+) ore and (\\d+) obsidian.\
       ",
    )
    .unwrap()
});

fn parse_input(input: &str) -> Vec<Blueprint> {
    input
        .trim()
        .lines()
        .map(|s| Blueprint::from_str(s).unwrap())
        .collect()
}

#[derive(Debug, Clone)]
struct OreRobot {
    ore: u16,
}

impl OreRobot {
    fn can_build(&self, resources: &[u16]) -> bool {
        self.ore <= resources[IND_ORE]
    }

    fn build(&self, resources: &[u16; 4], robots: &[u16; 4]) -> ([u16; 4], [u16; 4]) {
        let mut resources = resources.clone();
        resources[IND_ORE] -= self.ore;
        let mut robots = robots.clone();
        robots[IND_ORE] += 1;
        (resources, robots)
    }
}

#[derive(Debug, Clone)]
struct ClayRobot {
    ore: u16,
}

impl ClayRobot {
    fn can_build(&self, resources: &[u16]) -> bool {
        self.ore <= resources[IND_ORE]
    }

    fn build(&self, resources: &[u16; 4], robots: &[u16; 4]) -> ([u16; 4], [u16; 4]) {
        let mut resources = resources.clone();
        resources[IND_ORE] -= self.ore;
        let mut robots = robots.clone();
        robots[IND_CLY] += 1;
        (resources, robots)
    }
}

#[derive(Debug, Clone)]
struct ObsidianRobot {
    ore: u16,
    clay: u16,
}

impl ObsidianRobot {
    fn can_build(&self, resources: &[u16]) -> bool {
        self.ore <= resources[IND_ORE] && self.clay <= resources[IND_CLY]
    }

    fn build(&self, resources: &[u16; 4], robots: &[u16; 4]) -> ([u16; 4], [u16; 4]) {
        let mut resources = resources.clone();
        resources[IND_ORE] -= self.ore;
        resources[IND_CLY] -= self.clay;
        let mut robots = robots.clone();
        robots[IND_OBS] += 1;
        (resources, robots)
    }
}

#[derive(Debug, Clone)]
struct GeodeRobot {
    ore: u16,
    obsidian: u16,
}

impl GeodeRobot {
    fn can_build(&self, resources: &[u16]) -> bool {
        self.ore <= resources[IND_ORE] && self.obsidian <= resources[IND_OBS]
    }

    fn build(&self, resources: &[u16; 4], robots: &[u16; 4]) -> ([u16; 4], [u16; 4]) {
        let mut resources = resources.clone();
        resources[IND_ORE] -= self.ore;
        resources[IND_OBS] -= self.obsidian;
        let mut robots = robots.clone();
        robots[IND_GEO] += 1;
        (resources, robots)
    }
}

#[derive(Debug, Clone)]
struct Blueprint {
    id: u16,
    max_resource: [u16; 3],
    ore_robot: OreRobot,
    clay_robot: ClayRobot,
    obsidian_robot: ObsidianRobot,
    geode_robot: GeodeRobot,
}

impl FromStr for Blueprint {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let capture = REGEX
            .captures(s)
            .ok_or_else(|| format!("failed to parse: {}", s))?;

        let id = capture[1]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[1]))?;

        let ore_robot_cost = capture[2]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[2]))?;
        let ore_robot = OreRobot {
            ore: ore_robot_cost,
        };

        let ore_clay_cost = capture[3]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[3]))?;
        let clay_robot = ClayRobot { ore: ore_clay_cost };

        let ore_obsidian_cost = capture[4]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[4]))?;
        let clay_obsidian_cost = capture[5]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[5]))?;
        let obsidian_robot = ObsidianRobot {
            ore: ore_obsidian_cost,
            clay: clay_obsidian_cost,
        };

        let ore_geode_cost = capture[6]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[6]))?;
        let obsidian_geode_cost = capture[7]
            .parse()
            .map_err(|_| format!("failed to parse capture: {}", &capture[7]))?;
        let geode_robot = GeodeRobot {
            ore: ore_geode_cost,
            obsidian: obsidian_geode_cost,
        };

        let mut max_resource = [0; 3];
        max_resource[IND_ORE] = ore_robot
            .ore
            .max(clay_robot.ore)
            .max(obsidian_robot.ore)
            .max(geode_robot.ore);
        max_resource[IND_CLY] = obsidian_robot.clay;
        max_resource[IND_OBS] = geode_robot.obsidian;

        Ok(Blueprint {
            id,
            max_resource,
            ore_robot,
            clay_robot,
            obsidian_robot,
            geode_robot,
        })
    }
}

struct State {
    minutes: u16,
    robots: [u16; 4],
    resources: [u16; 4],
    mined: [u16; 4],
}

impl State {
    fn heuristics(&self) -> u16 {
        1000 * self.mined[IND_GEO]
            + 100 * self.mined[IND_OBS]
            + 10 * self.mined[IND_CLY]
            + self.mined[IND_ORE]
    }

    fn new_resources(&self) -> [u16; 4] {
        [
            self.resources[IND_ORE] + self.robots[IND_ORE],
            self.resources[IND_CLY] + self.robots[IND_CLY],
            self.resources[IND_OBS] + self.robots[IND_OBS],
            self.resources[IND_GEO] + self.robots[IND_GEO],
        ]
    }

    fn new_mined(&self) -> [u16; 4] {
        [
            self.mined[IND_ORE] + self.robots[IND_ORE],
            self.mined[IND_CLY] + self.robots[IND_CLY],
            self.mined[IND_OBS] + self.robots[IND_OBS],
            self.mined[IND_GEO] + self.robots[IND_GEO],
        ]
    }
}

fn bfs(blueprint: &Blueprint, max_minutes: u16, truncate_len: usize) -> u16 {
    let mut queue: VecDeque<State> = VecDeque::new();
    queue.push_back(State {
        minutes: 0,
        robots: [1, 0, 0, 0],
        resources: [0, 0, 0, 0],
        mined: [0, 0, 0, 0],
    });
    let mut max_geode_mined = 0;
    let mut depth = 0;

    while let Some(state) = queue.pop_front() {
        if state.minutes > depth {
            // prune search space !!!
            queue
                .make_contiguous()
                .sort_by(|a, b| b.heuristics().cmp(&a.heuristics()));
            queue.truncate(truncate_len);
            depth = state.minutes;
        }

        if state.minutes == max_minutes {
            max_geode_mined = std::cmp::max(max_geode_mined, state.mined[IND_GEO]);
            continue;
        }

        // mine ore with the robots
        let new_resources = state.new_resources();
        let new_mined = state.new_mined();

        // case of not building a robot
        queue.push_back(State {
            minutes: state.minutes + 1,
            robots: state.robots,
            resources: new_resources,
            mined: new_mined,
        });

        // if state has less ore than needed, try to build one
        if blueprint.max_resource[IND_ORE] > state.robots[IND_ORE] {
            if blueprint.ore_robot.can_build(&state.resources) {
                let (new_resources, new_robots) =
                    blueprint.ore_robot.build(&new_resources, &state.robots);
                queue.push_back(State {
                    minutes: state.minutes + 1,
                    robots: new_robots,
                    resources: new_resources,
                    mined: new_mined,
                });
            }
        }

        // if state has less clay than needed, try to build one
        if blueprint.max_resource[IND_CLY] > state.robots[IND_CLY] {
            if blueprint.clay_robot.can_build(&state.resources) {
                let (new_resources, new_robots) =
                    blueprint.clay_robot.build(&new_resources, &state.robots);
                queue.push_back(State {
                    minutes: state.minutes + 1,
                    robots: new_robots,
                    resources: new_resources,
                    mined: new_mined,
                });
            }
        }

        // if state has less obsidian than needed, try to build one
        if blueprint.max_resource[IND_OBS] > state.robots[IND_OBS] {
            if blueprint.obsidian_robot.can_build(&state.resources) {
                let (new_resources, new_robots) = blueprint
                    .obsidian_robot
                    .build(&new_resources, &state.robots);
                queue.push_back(State {
                    minutes: state.minutes + 1,
                    robots: new_robots,
                    resources: new_resources,
                    mined: new_mined,
                });
            }
        }

        // always try to buid geode
        if blueprint.geode_robot.can_build(&state.resources) {
            let (new_resources, new_robots) =
                blueprint.geode_robot.build(&new_resources, &state.robots);
            queue.push_back(State {
                minutes: state.minutes + 1,
                robots: new_robots,
                resources: new_resources,
                mined: new_mined,
            });
        }
    }

    max_geode_mined
}

fn part1(input: &[Blueprint]) -> u16 {
    let mut result = 0;
    for blueprint in input.iter() {
        let num_mined = bfs(blueprint, 24, 1000);
        result += blueprint.id * num_mined;
        // println!(
        //     "blueprint {:?}: mined geodes: {:?}",
        //     blueprint.id, num_mined
        // );
    }
    result
}

fn part2(input: &[Blueprint]) -> u16 {
    let mut result = 1;
    for blueprint in input.iter().take(3) {
        let num_mined = bfs(blueprint, 32, 10_000);
        result *= num_mined;
        // println!(
        //     "blueprint {:?}: mined geodes: {:?}",
        //     blueprint.id, num_mined
        // );
    }
    result
}

fn main() {
    let input = parse_input(include_str!("../input/day19.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
