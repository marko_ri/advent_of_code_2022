use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, Clone)]
enum Operation {
    Mul,
    Plus,
}

#[derive(Debug, Clone)]
enum Operand {
    Val(u64),
    Old,
}
#[derive(Debug, Clone)]
struct Monkey {
    items: Vec<u64>,
    operation: Operation,
    operand: Operand,
    divisible: u64,
    if_true: u64,
    if_false: u64,
    inspection: u64,
}

type MonkeyLink = Rc<RefCell<Monkey>>;

impl Monkey {
    fn apply(&self, x: u64) -> u64 {
        match self.operation {
            Operation::Plus => match self.operand {
                Operand::Val(val) => x + val,
                Operand::Old => x + x,
            },
            Operation::Mul => match self.operand {
                Operand::Val(val) => x * val,
                Operand::Old => x * x,
            },
        }
    }
}

impl From<&str> for Monkey {
    fn from(paragraph: &str) -> Self {
        let mut line_iter = paragraph.lines();
        let items: Vec<u64> = line_iter
            .nth(1)
            .unwrap()
            .split(':')
            .last()
            .unwrap()
            .split(',')
            .map(|d| d.trim().parse().unwrap())
            .collect();
        let mut operation_iter = line_iter
            .next()
            .unwrap()
            .split('=')
            .last()
            .unwrap()
            .split_whitespace();
        let operation = match operation_iter.nth(1) {
            Some("*") => Operation::Mul,
            Some("+") => Operation::Plus,
            e => panic!("{e:?}"),
        };
        let operand = match operation_iter.next() {
            Some("old") => Operand::Old,
            Some(x) => Operand::Val(x.parse::<u64>().unwrap()),
            e => panic!("{e:?}"),
        };
        let divisible: u64 = line_iter
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();
        let if_true: u64 = line_iter
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();
        let if_false: u64 = line_iter
            .next()
            .unwrap()
            .split_whitespace()
            .last()
            .unwrap()
            .parse()
            .unwrap();

        Self {
            items,
            operation,
            operand,
            divisible,
            if_true,
            if_false,
            inspection: 0,
        }
    }
}

fn parse_input(input: &str) -> Vec<Monkey> {
    input
        .trim()
        .split("\n\n")
        .map(|paragraph| Monkey::from(paragraph))
        .collect()
}

fn part1(input: &[MonkeyLink]) -> u64 {
    let len = input.len();
    for _round in 0..20 {
        for i in 0..len {
            let mut monkey = input[i].borrow_mut();
            for item in monkey.items.iter() {
                let worry = monkey.apply(*item) / 3;
                let ind = if worry % monkey.divisible == 0 {
                    monkey.if_true
                } else {
                    monkey.if_false
                };
                input[ind as usize].borrow_mut().items.push(worry);
            }

            monkey.inspection += monkey.items.len() as u64;
            monkey.items.clear();
        }
    }

    let mut inspections: Vec<u64> = input.iter().map(|m| m.borrow().inspection).collect();
    inspections.sort_unstable_by(|a, b| b.cmp(a));
    inspections[0] * inspections[1]
}

fn part2(input: &[MonkeyLink]) -> u64 {
    // because monkey.divisible are prime numbers
    let mut big_mod = 1;
    for ml in input.iter() {
        big_mod *= ml.borrow().divisible;
    }

    let len = input.len();
    for _round in 0..10_000 {
        for i in 0..len {
            let mut monkey = input[i].borrow_mut();
            for item in monkey.items.iter() {
                let worry = monkey.apply(*item) % big_mod;
                let ind = if worry % monkey.divisible == 0 {
                    monkey.if_true
                } else {
                    monkey.if_false
                };
                input[ind as usize].borrow_mut().items.push(worry);
            }

            monkey.inspection += monkey.items.len() as u64;
            monkey.items.clear();
        }
    }

    let mut inspections: Vec<u64> = input.iter().map(|m| m.borrow().inspection).collect();
    inspections.sort_unstable_by(|a, b| b.cmp(a));
    inspections[0] * inspections[1]
}

fn main() {
    let input = parse_input(include_str!("../input/day11.txt"));
    let input_link1: Vec<MonkeyLink> = input
        .iter()
        .map(|m| Rc::new(RefCell::new(m.clone())))
        .collect();
    let part1 = part1(&input_link1);
    println!("part1: {:?}", part1);
    let input_link2: Vec<MonkeyLink> = input
        .into_iter()
        .map(|m| Rc::new(RefCell::new(m)))
        .collect();
    let part2 = part2(&input_link2);
    println!("part2: {:?}", part2);
}
