mod shared;
use core::cmp::Ordering;
use shared::Direction;
use std::collections::{BinaryHeap, HashMap, HashSet};

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct Blizzard {
    coord: (i32, i32),
    dir: Direction,
}

impl Blizzard {
    fn contains_coord(&self, coord: (i32, i32)) -> bool {
        self.coord == coord
    }
}

fn parse_input(input: &str) -> (HashSet<Blizzard>, i32, i32) {
    let rows = input.lines().count();
    let cols = input.lines().next().unwrap().bytes().count();
    let mut blizzard = HashSet::new();

    for (row, line) in input.lines().enumerate() {
        for (col, b) in line.bytes().enumerate() {
            if b == b'.' || b == b'#' {
                continue;
            }
            let coord = (row as i32, col as i32);
            let arrow = match b {
                b'^' => Blizzard {
                    coord,
                    dir: Direction::Up,
                },
                b'v' => Blizzard {
                    coord,
                    dir: Direction::Down,
                },
                b'<' => Blizzard {
                    coord,
                    dir: Direction::Left,
                },
                b'>' => Blizzard {
                    coord,
                    dir: Direction::Right,
                },
                _ => panic!("invalid input"),
            };
            blizzard.insert(arrow);
        }
    }

    (blizzard, rows as i32, cols as i32)
}

// greatest common divisor
fn gcd(a: i32, b: i32) -> i32 {
    let mut a = a.abs();
    let mut b = b.abs();

    while b != 0 {
        let t = b;
        b = a % b;
        a = t;
    }

    a
}

// least common multiple
fn lcm(a: i32, b: i32) -> i32 {
    (a * b).abs() / gcd(a, b)
}

// The blizzard positions repeat after 'lcm' minutes (period)
fn precompute_blizzards(
    blizzards: &HashSet<Blizzard>,
    rows: i32,
    cols: i32,
) -> HashMap<i32, HashSet<Blizzard>> {
    let period = lcm(rows - 2, cols - 2);
    let mut result = HashMap::new();
    result.insert(0, blizzards.clone());

    for time in 1..period {
        let mut new_blizzards = HashSet::new();
        for b in result.get(&(time - 1)).unwrap() {
            let (row, col) = b.coord;
            let (drow, dcol) = b.dir.diff();
            let mut new_row = row + drow;
            let mut new_col = col + dcol;

            if new_row == 0 {
                new_row = rows - 2;
            } else if new_row == rows - 1 {
                new_row = 1;
            }

            if new_col == 0 {
                new_col = cols - 2;
            } else if new_col == cols - 1 {
                new_col = 1;
            }

            new_blizzards.insert(Blizzard {
                coord: (new_row, new_col),
                dir: b.dir.clone(),
            });
        }

        result.insert(time, new_blizzards);
    }

    result
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
struct PqNode {
    time: i32,
    coord: (i32, i32),
}

impl Ord for PqNode {
    // sorted so nodes with lowest time get popped off the queue first
    fn cmp(&self, other: &Self) -> Ordering {
        other.time.cmp(&self.time)
    }
}

impl PartialOrd for PqNode {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn run(
    (blizzards, rows, cols): &(HashSet<Blizzard>, i32, i32),
    start_time: i32,
    start: (i32, i32),
    end: (i32, i32),
) -> i32 {
    let period = lcm(rows - 2, cols - 2);
    let precomputed_map = precompute_blizzards(blizzards, *rows, *cols);

    let mut pq: BinaryHeap<PqNode> = BinaryHeap::new();
    pq.push(PqNode {
        time: start_time,
        coord: start,
    });
    let mut visited: HashSet<PqNode> = HashSet::new();

    while let Some(pq_node) = pq.pop() {
        if visited.contains(&pq_node) {
            continue;
        }
        visited.insert(pq_node.clone());

        if pq_node.coord == end {
            return pq_node.time;
        }

        // go through neighbors, plus posibility you do not move
        for (drow, dcol) in Direction::VALUES
            .iter()
            .map(|d| d.diff())
            .chain([(0, 0)].into_iter())
        {
            let new_row = pq_node.coord.0 + drow;
            let new_col = pq_node.coord.1 + dcol;
            let new_coord = (new_row, new_col);

            // within bounds?
            if new_coord != start
                && new_coord != end
                && (new_row < 1 || new_row > rows - 2 || new_col < 1 || new_col > cols - 2)
            {
                continue;
            }

            // check if hitting a blizzard
            if let Some(blizzard) = precomputed_map.get(&((pq_node.time + 1) % period)) {
                if blizzard.iter().any(|b| b.contains_coord(new_coord)) {
                    continue;
                }
            }

            pq.push(PqNode {
                time: pq_node.time + 1,
                coord: new_coord,
            });
        }
    }

    unreachable!()
}

fn part1(input: &(HashSet<Blizzard>, i32, i32)) -> i32 {
    let (_b, rows, cols) = input;
    let start = (0, 1);
    let end = (rows - 1, cols - 2);
    run(input, 0, start, end)
}

fn part2(input: &(HashSet<Blizzard>, i32, i32)) -> i32 {
    let (_b, rows, cols) = input;
    let start = (0, 1);
    let end = (rows - 1, cols - 2);
    let there = run(input, 0, start, end);
    let back = run(input, there, end, start);
    run(input, back, start, end)
}

fn main() {
    let input = parse_input(include_str!("../input/day24.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
