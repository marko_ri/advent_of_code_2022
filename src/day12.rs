mod shared;
use crate::shared::Direction;
use std::collections::{HashSet, VecDeque};

#[derive(Default, PartialEq, Eq, Hash, Clone, Debug)]
struct Coord {
    x: i32,
    y: i32,
}

impl Coord {
    fn is_bounded(&self, (rows, cols): (usize, usize)) -> bool {
        self.x >= 0 && self.x < cols as i32 && self.y >= 0 && self.y < rows as i32
    }
}

#[derive(Debug)]
struct Input {
    grid: Vec<Vec<u8>>,
    start: Coord,
    end: Coord,
    rows: usize,
    cols: usize,
}

impl Input {
    fn from(input: &str) -> Self {
        let rows = input.lines().count();
        let cols = input.lines().next().unwrap().len();
        let mut start = Coord::default();
        let mut end = Coord::default();
        let mut grid = vec![vec![0; cols]; rows];

        for (row, line) in input.lines().enumerate() {
            for (col, c) in line.as_bytes().iter().enumerate() {
                let letter = match c {
                    b'S' => {
                        start.x = col as i32;
                        start.y = row as i32;
                        b'a'
                    }
                    b'E' => {
                        end.x = col as i32;
                        end.y = row as i32;
                        b'z'
                    }
                    b'a'..=b'z' => *c,
                    _ => panic!("invalid input"),
                };
                grid[row][col] = letter - b'a';
            }
        }

        Self {
            grid,
            start,
            end,
            rows,
            cols,
        }
    }

    fn neighbors_heigher(&self, pos: &Coord) -> Vec<Coord> {
        Direction::VALUES
            .iter()
            .map(|d| d.diff())
            .map(|diff| Coord {
                x: pos.x + diff.1,
                y: pos.y + diff.0,
            })
            .filter(|nbor| nbor.is_bounded((self.rows, self.cols)))
            .filter(|nbor| {
                self.grid[nbor.y as usize][nbor.x as usize]
                    <= self.grid[pos.y as usize][pos.x as usize] + 1
            })
            .collect()
    }

    fn neighbors_lower(&self, pos: &Coord) -> Vec<Coord> {
        Direction::VALUES
            .iter()
            .map(|d| d.diff())
            .map(|diff| Coord {
                x: pos.x + diff.1,
                y: pos.y + diff.0,
            })
            .filter(|nbor| nbor.is_bounded((self.rows, self.cols)))
            .filter(|nbor| {
                self.grid[nbor.y as usize][nbor.x as usize]
                    >= self.grid[pos.y as usize][pos.x as usize] - 1
            })
            .collect()
    }
}

fn parse_input(input: &str) -> Input {
    Input::from(input)
}

fn part1(input: &Input) -> u32 {
    let mut visited: HashSet<Coord> = HashSet::new();
    let mut queue: VecDeque<(u32, Coord)> = VecDeque::new();
    queue.push_back((0, input.start.clone()));

    while let Some((steps, pos)) = queue.pop_front() {
        if visited.contains(&pos) {
            continue;
        }
        visited.insert(pos.clone());

        if pos == input.end {
            return steps;
        }

        for nbor in input.neighbors_heigher(&pos).into_iter() {
            queue.push_back((steps + 1, nbor));
        }
    }
    0
}

fn part2(input: &Input) -> u32 {
    let mut visited: HashSet<Coord> = HashSet::new();
    let mut queue: VecDeque<(u32, Coord)> = VecDeque::new();
    queue.push_back((0, input.end.clone()));

    while let Some((steps, pos)) = queue.pop_front() {
        if visited.contains(&pos) {
            continue;
        }
        visited.insert(pos.clone());

        if input.grid[pos.y as usize][pos.x as usize] == 0 {
            return steps;
        }
        for nbor in input.neighbors_lower(&pos).into_iter() {
            queue.push_back((steps + 1, nbor));
        }
    }
    0
}

fn main() {
    let input = parse_input(include_str!("../input/day12.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
