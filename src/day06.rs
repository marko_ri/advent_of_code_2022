use std::collections::HashSet;

fn run(input: &str, step: usize) -> usize {
    for i in 0..input.len() - step {
        let set: HashSet<char> = input[i..i + step].chars().collect();
        if set.len() == step {
            return i + step;
        }
    }
    0
}

fn main() {
    let input = include_str!("../input/day06.txt");
    let part1 = run(&input, 4);
    println!("part1: {:?}", part1);
    let part2 = run(&input, 14);
    println!("part2: {:?}", part2);
}
