use once_cell::sync::Lazy;
use std::collections::{HashMap, HashSet};

#[derive(Default, PartialEq, Eq, Hash, Clone)]
struct Coord {
    x: i32,
    y: i32,
}

impl Coord {
    fn is_touching(&self, other: &Coord) -> bool {
        self.x.abs_diff(other.x) <= 1 && self.y.abs_diff(other.y) <= 1
    }

    fn add_mut(&mut self, other: &Coord) {
        self.x += other.x;
        self.y += other.y;
    }

    fn distance(&self, other: &Coord) -> Coord {
        if !self.is_touching(other) {
            let x = if self.x == other.x {
                0
            } else {
                (self.x - other.x) / self.x.abs_diff(other.x) as i32
            };
            let y = if self.y == other.y {
                0
            } else {
                (self.y - other.y) / self.y.abs_diff(other.y) as i32
            };
            Coord { x, y }
        } else {
            Coord::default()
        }
    }
}

static DIFF_MAP: Lazy<HashMap<u8, Coord>> = Lazy::new(|| {
    HashMap::from([
        (b'R', Coord { x: 1, y: 0 }),
        (b'U', Coord { x: 0, y: 1 }),
        (b'L', Coord { x: -1, y: 0 }),
        (b'D', Coord { x: 0, y: -1 }),
    ])
});

fn parse_input(input: &str) -> Vec<(u8, i32)> {
    input
        .lines()
        .map(|l| {
            let (dir, amount) = l.split_once(' ').unwrap();
            let dir = dir.as_bytes()[0];
            let amount = amount.parse().unwrap();
            (dir, amount)
        })
        .collect()
}

fn part1(input: &[(u8, i32)]) -> u32 {
    let mut head = Coord::default();
    let mut tail = Coord::default();
    let mut tail_visited = HashSet::new();
    tail_visited.insert(tail.clone());

    for (dir, amount) in input.iter() {
        let diff = DIFF_MAP.get(&dir).unwrap();
        for _ in 0..*amount {
            head.add_mut(diff);
            let tail_dist = head.distance(&tail);
            tail.add_mut(&tail_dist);
            tail_visited.insert(tail.clone());
        }
    }

    tail_visited.len() as u32
}

fn part2(input: &[(u8, i32)]) -> u32 {
    let mut knots = vec![Coord::default(); 10];
    let mut tail_visited = HashSet::new();
    tail_visited.insert(Coord::default());

    for (dir, amount) in input.iter() {
        let diff = DIFF_MAP.get(&dir).unwrap();
        for _ in 0..*amount {
            let first = &mut knots[0];
            first.add_mut(diff);
            for i in 1..10 {
                let head = &knots[i - 1];
                let tail = &knots[i];
                let tail_dist = head.distance(tail);
                let tail = &mut knots[i];
                tail.add_mut(&tail_dist);
            }
            tail_visited.insert(knots.last().unwrap().clone());
        }
    }

    tail_visited.len() as u32
}

fn main() {
    let input = parse_input(include_str!("../input/day09.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
