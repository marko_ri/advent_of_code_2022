fn parse_input(input: &str) -> Vec<(u8, u8)> {
    let mut result = Vec::new();
    for line in input.lines() {
        for range in line.split(',') {
            let mut nums = range.split('-');
            result.push((
                nums.next().unwrap().parse().unwrap(),
                nums.next().unwrap().parse().unwrap(),
            ));
        }
    }
    result
}

fn part1(input: &[(u8, u8)]) -> u32 {
    let mut result = 0;
    for range in input.chunks(2) {
        let (a, b) = range[0];
        let (c, d) = range[1];
        if (a <= c && b >= d) || (a >= c && b <= d) {
            result += 1;
        }
    }
    result
}

fn part2(input: &[(u8, u8)]) -> u32 {
    let mut result = 0;
    for range in input.chunks(2) {
        let (a, b) = range[0];
        let (c, d) = range[1];
        if !(b < c || a > d) {
            result += 1;
        }
    }
    result
}

fn main() {
    let input = parse_input(include_str!("../input/day04.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
