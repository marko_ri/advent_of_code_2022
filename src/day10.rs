const COLS: usize = 40;
const ROWS: usize = 6;
const SPRITE_WIDTH: u32 = 3;

fn part1(input: &str) -> i32 {
    let mut result = 0;
    let mut x = 1;
    let mut time = 1;

    for line in input.lines() {
        if time % 40 == 20 {
            result += time * x;
        }
        time += 1;

        if let Some(("addx", num)) = line.split_once(' ') {
            if time % 40 == 20 {
                result += time * x;
            }
            let num: i32 = num.parse().unwrap();
            x += num;
            time += 1;
        }
    }

    result
}

fn get_pixel(time: usize, x: i32) -> char {
    let curr_col = (time - 1) % COLS;
    if (curr_col as i32).abs_diff(x) <= SPRITE_WIDTH / 2 {
        '#'
    } else {
        ' '
    }
}

fn part2(input: &str) -> String {
    let mut x = 1;
    let mut time = 1;
    let mut screen = [' '; COLS * ROWS];

    for line in input.lines() {
        screen[time - 1] = get_pixel(time, x);
        time += 1;

        if let Some(("addx", num)) = line.split_once(' ') {
            screen[time - 1] = get_pixel(time, x);
            time += 1;
            let num: i32 = num.parse().unwrap();
            x += num;
        }
    }

    let mut result = String::from("");
    for row in 0..ROWS {
        for col in 0..COLS {
            result.push(screen[row * 40 + col]);
        }
        result.push('\n');
    }

    result
}

fn main() {
    let input = include_str!("../input/day10.txt");
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("{part2}");
}
