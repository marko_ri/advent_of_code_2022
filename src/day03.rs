fn priority(item: u8) -> u8 {
    match item {
        b'a'..=b'z' => (item - b'a') + 1,
        b'A'..=b'Z' => (item - b'A') + 1 + 26,
        _ => panic!("only a-z and A-Z allowed"),
    }
}

fn part1(input: &str) -> u32 {
    input
        .lines()
        .map(|line| line.as_bytes())
        .filter_map(|line| {
            let (left, right) = line.split_at(line.len() / 2);
            left.iter()
                .find(|c| right.contains(c))
                .map(|c| priority(*c) as u32)
        })
        .sum()
}

fn part2(input: &str) -> u32 {
    let input: Vec<&[u8]> = input.lines().map(|line| line.as_bytes()).collect();

    input
        .chunks(3)
        .filter_map(|chunk| {
            let (line1, line2, line3) = (chunk[0], chunk[1], chunk[2]);
            line1
                .iter()
                .find(|c| line2.contains(c) && line3.contains(c))
                .map(|c| priority(*c) as u32)
        })
        .sum()
}

fn main() {
    let input = include_str!("../input/day03.txt");
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
