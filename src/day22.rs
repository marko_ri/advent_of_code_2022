#[derive(Debug, Clone, PartialEq, Eq)]
enum Move {
    Turn(Rotation),
    Forward(u8),
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum Rotation {
    Cw,
    Ccw,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Clone)]
struct State {
    x: usize,
    y: usize,
    dir: Direction,
}

struct Input {
    grid: Vec<Vec<u8>>,
    moves: Vec<Move>,
}

fn parse_input(input: &str) -> Input {
    let mut lines = input.lines();
    let grid: Vec<Vec<u8>> = lines
        .by_ref()
        .take_while(|l| !l.is_empty())
        .map(|l| l.as_bytes().to_vec())
        .collect();

    let moves_str = lines.next().expect("moves after empty line");
    let moves = parse_moves(moves_str);

    Input { grid, moves }
}

fn parse_moves(moves: &str) -> Vec<Move> {
    let mut result = Vec::new();
    let mut iter = moves.chars().peekable();
    while let Some(ch) = iter.next() {
        match ch {
            'R' => {
                result.push(Move::Turn(Rotation::Cw));
            }
            'L' => {
                result.push(Move::Turn(Rotation::Ccw));
            }
            _ => {
                let mut digits = String::from(ch);
                while let Some(d) = iter.next_if(|&c| c.is_digit(10)) {
                    digits.push(d);
                }
                result.push(Move::Forward(digits.parse().unwrap()));
            }
        }
    }

    result
}

fn solve<F>(Input { grid, moves }: &Input, move_one: F) -> usize
where
    F: Fn(&State) -> State,
{
    let mut state = State {
        x: grid[0].iter().position(|&c| c == b'.').unwrap(),
        y: 0,
        dir: Direction::Right,
    };

    for mov in moves.iter() {
        state = match mov {
            Move::Turn(Rotation::Cw) => State {
                dir: match state.dir {
                    Direction::Up => Direction::Right,
                    Direction::Right => Direction::Down,
                    Direction::Down => Direction::Left,
                    Direction::Left => Direction::Up,
                },
                ..state
            },
            Move::Turn(Rotation::Ccw) => State {
                dir: match state.dir {
                    Direction::Up => Direction::Left,
                    Direction::Left => Direction::Down,
                    Direction::Down => Direction::Right,
                    Direction::Right => Direction::Up,
                },
                ..state
            },
            Move::Forward(n) => {
                let mut prev_state = state.clone();
                let mut prev_valid_state = state;
                let mut next_state;
                let mut step = 0;
                loop {
                    next_state = move_one(&prev_state);
                    if *grid[next_state.y].get(next_state.x).unwrap_or(&b' ') == b' ' {
                        prev_state = next_state;
                        continue;
                    }

                    if grid[next_state.y][next_state.x] == b'#' {
                        next_state = prev_valid_state;
                        break;
                    }
                    step += 1;
                    if step == *n {
                        break;
                    }
                    prev_valid_state = next_state.clone();
                    prev_state = next_state;
                }
                next_state
            }
        };
    }

    let row_number = state.y + 1;
    let col_number = state.x + 1;
    let facing_number = match state.dir {
        Direction::Right => 0,
        Direction::Down => 1,
        Direction::Left => 2,
        Direction::Up => 3,
    };

    1000 * row_number + 4 * col_number + facing_number
}

fn part1(input: &Input) -> usize {
    solve(input, move_one_2d::<150, 200>)
}

fn part2(input: &Input) -> usize {
    solve(input, move_one_cube)
}

fn main() {
    let input = parse_input(include_str!("../input/day22.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", &part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}

fn move_one_2d<const X: usize, const Y: usize>(state: &State) -> State {
    match state.dir {
        Direction::Right => State {
            x: (state.x + 1) % X,
            y: state.y,
            dir: state.dir,
        },
        Direction::Down => State {
            x: state.x,
            y: (state.y + 1) % Y,
            dir: state.dir,
        },
        Direction::Left => State {
            x: state.x.checked_sub(1).unwrap_or(X - 1),
            y: state.y,
            dir: state.dir,
        },
        Direction::Up => State {
            x: state.x,
            y: state.y.checked_sub(1).unwrap_or(Y - 1),
            dir: state.dir,
        },
    }
}

fn move_one_cube(&State { x, y, dir }: &State) -> State {
    match dir {
        Direction::Right => match (x, y) {
            (149, 0..=49) => State {
                x: 99,
                y: 149 - y,
                dir: Direction::Left,
            },
            (99, 50..=99) => State {
                x: 100 + (y - 50),
                y: 49,
                dir: Direction::Up,
            },
            (99, 100..=149) => State {
                x: 149,
                y: 49 - (y - 100),
                dir: Direction::Left,
            },
            (49, 150..=199) => State {
                x: 50 + (y - 150),
                y: 149,
                dir: Direction::Up,
            },
            _ => State { x: x + 1, y, dir },
        },
        Direction::Left => match (x, y) {
            (50, 0..=49) => State {
                x: 0,
                y: 149 - y,
                dir: Direction::Right,
            },
            (50, 50..=99) => State {
                x: y - 50,
                y: 100,
                dir: Direction::Down,
            },
            (0, 100..=149) => State {
                x: 50,
                y: 49 - (y - 100),
                dir: Direction::Right,
            },
            (0, 150..=199) => State {
                x: 50 + (y - 150),
                y: 0,
                dir: Direction::Down,
            },
            _ => State { x: x - 1, y, dir },
        },
        Direction::Down => match (x, y) {
            (0..=49, 199) => State {
                x: x + 100,
                y: 0,
                dir: Direction::Down,
            },
            (50..=99, 149) => State {
                x: 49,
                y: 150 + (x - 50),
                dir: Direction::Left,
            },
            (100..=149, 49) => State {
                x: 99,
                y: 50 + (x - 100),
                dir: Direction::Left,
            },
            _ => State { x, y: y + 1, dir },
        },
        Direction::Up => match (x, y) {
            (0..=49, 100) => State {
                x: 50,
                y: 50 + x,
                dir: Direction::Right,
            },
            (50..=99, 0) => State {
                x: 0,
                y: 150 + (x - 50),
                dir: Direction::Right,
            },
            (100..=149, 0) => State {
                x: x - 100,
                y: 199,
                dir: Direction::Up,
            },
            _ => State { x, y: y - 1, dir },
        },
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let expected = vec![
            Move::Turn(Rotation::Ccw),
            Move::Forward(12),
            Move::Turn(Rotation::Cw),
            Move::Forward(255),
        ];
        let actual = parse_moves("L12R255");
        assert_eq!(expected, actual);
    }
}
