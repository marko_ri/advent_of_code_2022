use std::collections::HashSet;

fn parse_input(input: &str) -> HashSet<(i16, i16, i16)> {
    let mut result = HashSet::new();
    for line in input.lines() {
        let mut nums = line.split(',');
        result.insert((
            nums.next().unwrap().parse().unwrap(),
            nums.next().unwrap().parse().unwrap(),
            nums.next().unwrap().parse().unwrap(),
        ));
    }

    result
}

fn neighbors((x, y, z): &(i16, i16, i16)) -> [(i16, i16, i16); 6] {
    [
        (*x + 1, *y, *z),
        (*x - 1, *y, *z),
        (*x, *y + 1, *z),
        (*x, *y - 1, *z),
        (*x, *y, *z + 1),
        (*x, *y, *z - 1),
    ]
}

fn part1(input: &HashSet<(i16, i16, i16)>) -> usize {
    let mut result = 0;
    for pos in input.iter() {
        let covered = neighbors(pos).iter().filter(|n| input.contains(n)).count();
        result += 6 - covered;
    }
    result
}

fn exposed(
    pos: (i16, i16, i16),
    min_coord: i16,
    max_coord: i16,
    input: &HashSet<(i16, i16, i16)>,
) -> bool {
    if input.contains(&pos) {
        return false;
    }

    let mut stack = vec![pos];
    let mut seen = HashSet::new();

    while let Some(pop) = stack.pop() {
        if input.contains(&pop) {
            continue;
        }

        if pop.0 < min_coord
            || pop.0 > max_coord
            || pop.1 < min_coord
            || pop.1 > max_coord
            || pop.2 < min_coord
            || pop.2 > max_coord
        {
            return true;
        }

        if !seen.insert(pop) {
            continue;
        }

        for neighbor in neighbors(&pop) {
            stack.push(neighbor);
        }
    }

    false
}

fn part2(input: &HashSet<(i16, i16, i16)>) -> usize {
    let mut min_coord = i16::MAX;
    let mut max_coord = i16::MIN;
    let mut result = 0;

    for (x, y, z) in input.iter() {
        min_coord = min_coord.min(*x).min(*y).min(*z);
        max_coord = max_coord.max(*x).max(*y).max(*z);
    }

    for pos in input.iter() {
        result += neighbors(pos)
            .into_iter()
            .filter(|&n| exposed(n, min_coord, max_coord, input))
            .count();
    }

    result
}

fn main() {
    let input = parse_input(include_str!("../input/day18.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
