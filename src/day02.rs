fn parse_input(input: &str) -> Vec<(u8, u8)> {
    input
        .lines()
        .map(|line| {
            let bytes = line.as_bytes();
            (bytes[0], bytes[2])
        })
        .collect()
}

fn part1(input: &[(u8, u8)]) -> i32 {
    input
        .iter()
        .map(|(opp, me)| {
            // transform A B C and X Y Z to 0 1 2 respectively by using
            // their ASCII order => 0 for rock, 1 for paper, 2 for scissors
            let opp = (opp - b'A') as i32;
            let me = (me - b'X') as i32;

            // 0 for loss, 1 for draw, 2 for win
            let outcome = (me - opp + 1).rem_euclid(3);

            let shape_score = me + 1;
            let outcome_score = 3 * outcome;

            shape_score + outcome_score
        })
        .sum()
}

fn part2(input: &[(u8, u8)]) -> i32 {
    input
        .iter()
        .map(|(opp, outcome)| {
            // transform A B C and X Y Z to 0 1 2 respectively by using
            // their ASCII order => 0 for rock, 1 for paper, 2 for scissors
            let opp = (opp - b'A') as i32;
            // 0 for loss, 1 for draw, 2 for win
            let outcome = (outcome - b'X') as i32;

            // solve the equation from part1
            let me = (opp - 1 + outcome).rem_euclid(3);

            let shape_score = me + 1;
            let outcome_score = 3 * outcome;

            shape_score + outcome_score
        })
        .sum()
}

fn main() {
    let input = parse_input(include_str!("../input/day02.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
