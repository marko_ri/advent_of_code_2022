use std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
enum Op {
    Sub,
    Add,
    Mul,
    Div,
}

impl Op {
    fn apply(self, x: i64, y: i64) -> i64 {
        match self {
            Op::Sub => x - y,
            Op::Add => x + y,
            Op::Mul => x * y,
            Op::Div => x / y,
        }
    }

    fn calc_left(self, val: i64, r: i64) -> i64 {
        match self {
            // l - r = val => l = val + r
            Op::Sub => val + r,
            // l + r = val => l = val - r
            Op::Add => val - r,
            // l * r = val => l = val / r
            Op::Mul => val / r,
            // l / r = val => l = val * r
            Op::Div => val * r,
        }
    }

    fn calc_right(self, val: i64, l: i64) -> i64 {
        match self {
            // l - r = val => r = l - val
            Op::Sub => l - val,
            // l + r = val => r = val - l
            Op::Add => val - l,
            // l * r = val => r = val / l
            Op::Mul => val / l,
            // l / r = val => r = l / val
            Op::Div => l / val,
        }
    }
}

#[derive(Debug)]
enum Node<'a> {
    Val(i64),
    Expr(&'a str, Op, &'a str),
}

fn parse_input(input: &str) -> HashMap<&str, Node> {
    let mut result = HashMap::new();

    for line in input.lines() {
        let parts: Vec<&str> = line.split_whitespace().collect();
        let key = parts[0].strip_suffix(':').unwrap();
        if parts.len() == 2 {
            let val = parts[1].parse::<i64>().unwrap();
            result.insert(key, Node::Val(val));
        } else {
            let l = parts[1];
            let op = match parts[2] {
                "+" => Op::Add,
                "-" => Op::Sub,
                "*" => Op::Mul,
                "/" => Op::Div,
                _ => unreachable!(),
            };
            let r = parts[3];
            result.insert(key, Node::Expr(l, op, r));
        }
    }

    result
}

fn compute(input: &HashMap<&str, Node>, name: &str) -> i64 {
    match input.get(name).unwrap() {
        Node::Val(val) => {
            return *val;
        }
        Node::Expr(l, op, r) => {
            let left = compute(input, l);
            let right = compute(input, r);
            return op.apply(left, right);
        }
    }
}

fn part1(input: &HashMap<&str, Node>) -> i64 {
    compute(input, "root")
}

fn depends_on_human(input: &HashMap<&str, Node>, name: &str) -> bool {
    if name == "humn" {
        return true;
    }
    match input.get(name).unwrap() {
        Node::Val(_) => false,
        Node::Expr(l, _op, r) => depends_on_human(input, l) || depends_on_human(input, r),
    }
}

fn compute_human(input: &HashMap<&str, Node>, name: &str, val: i64) -> i64 {
    if name == "humn" {
        return val;
    }

    match input.get(name).unwrap() {
        Node::Val(n) => *n,
        Node::Expr(l, op, r) => {
            let (new_name, new_val) = if depends_on_human(input, l) {
                let r_val = compute(input, r);
                let new_val = op.calc_left(val, r_val);
                (l, new_val)
            } else {
                let l_val = compute(input, l);
                let new_val = op.calc_right(val, l_val);
                (r, new_val)
            };

            compute_human(input, new_name, new_val)
        }
    }
}

fn part2(input: &HashMap<&str, Node>) -> i64 {
    // e.g.
    // aaa = bbb + ccc
    // aaa needs to be 10, bbb evaluates to 4, ccc depends on humn
    // => ccc = aaa - bbb, and recurse
    let Node::Expr(left, _op, right) = input.get("root").unwrap() else {
        panic!("invalid input");
    };
    let (name, val) = if depends_on_human(input, left) {
        (left, compute(input, right))
    } else {
        (right, compute(input, left))
    };

    compute_human(input, name, val)
}

fn main() {
    let input = parse_input(include_str!("../input/day21.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
