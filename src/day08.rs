mod shared;
use crate::shared::Direction;

struct Input {
    grid: Vec<Vec<u32>>,
}

impl Input {
    fn from(input: &str) -> Self {
        let grid = input
            .lines()
            .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect())
            .collect();
        Self { grid }
    }

    fn num_rows(&self) -> usize {
        self.grid.len()
    }

    fn num_cols(&self) -> usize {
        self.grid[0].len()
    }

    fn get(&self, row: usize, col: usize) -> u32 {
        self.grid[row][col]
    }

    fn all_up_are_less(&self, col: usize, row: usize, cur: u32) -> bool {
        self.grid
            .iter()
            .enumerate()
            .filter(|(c, _)| *c < row)
            .all(|(_, v)| v[col] < cur)
    }

    fn all_down_are_less(&self, col: usize, row: usize, cur: u32) -> bool {
        self.grid
            .iter()
            .enumerate()
            .filter(|(c, _)| *c > row)
            .all(|(_, v)| v[col] < cur)
    }

    fn all_left_are_less(&self, col: usize, row: usize, cur: u32) -> bool {
        self.grid[row][..col].iter().all(|&v| v < cur)
    }

    fn all_right_are_less(&self, col: usize, row: usize, cur: u32) -> bool {
        self.grid[row][col + 1..].iter().all(|&v| v < cur)
    }
}

fn parse_input(input: &str) -> Input {
    Input::from(input)
}

fn part1(input: &Input) -> u32 {
    let num_rows = input.num_rows();
    let num_cols = input.num_cols();
    let mut result = 0;

    for r in 0..num_rows {
        for c in 0..num_cols {
            let cur = input.get(r, c);
            if c == 0 || input.all_left_are_less(c, r, cur) {
                result += 1;
            } else if c == num_cols - 1 || input.all_right_are_less(c, r, cur) {
                result += 1;
            } else if r == 0 || input.all_up_are_less(c, r, cur) {
                result += 1;
            } else if r == num_rows - 1 || input.all_down_are_less(c, r, cur) {
                result += 1;
            }
        }
    }

    result
}

fn part2(input: &Input) -> u32 {
    let num_rows = input.num_rows();
    let num_cols = input.num_cols();
    let mut result = 0;
    let mut score;
    let mut distance;

    for r in 0..num_rows {
        for c in 0..num_cols {
            let cur = input.get(r, c);
            score = 1;
            // scan in 4 directions
            for (dr, dc) in Direction::VALUES.iter().map(|d| d.diff()) {
                let mut rr = r as i32 + dr;
                let mut cc = c as i32 + dc;
                distance = 0;

                while (0 <= rr && rr < num_rows as i32 && 0 <= cc && cc < num_cols as i32)
                    && input.get(rr as usize, cc as usize) < cur
                {
                    distance += 1;
                    rr += dr;
                    cc += dc;

                    if (0 <= rr && rr < num_rows as i32 && 0 <= cc && cc < num_cols as i32)
                        && input.get(rr as usize, cc as usize) >= cur
                    {
                        distance += 1;
                    }
                }

                score *= distance;
            }

            result = std::cmp::max(result, score);
        }
    }

    result
}

fn main() {
    let input = parse_input(include_str!("../input/day08.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
