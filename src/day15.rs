use regex::Regex;

#[derive(Debug)]
struct Coord {
    x: i32,
    y: i32,
}

impl Coord {
    fn dist(&self, other: &Coord) -> u32 {
        self.x.abs_diff(other.x) + self.y.abs_diff(other.y)
    }
}

#[derive(Debug)]
struct Input {
    sensors: Vec<Coord>,
    beacons: Vec<Coord>,
}

fn parse_input(input: &str) -> Input {
    let re = Regex::new(r"Sensor at x=(?P<sx>[+-]?\d+), y=(?P<sy>[+-]?\d+): closest beacon is at x=(?P<bx>[+-]?\d+), y=(?P<by>[+-]?\d+)").unwrap();
    let mut result = Input {
        sensors: vec![],
        beacons: vec![],
    };
    for line in input.trim().lines() {
        let caps = re.captures(line).unwrap();

        let sx = caps.name("sx").unwrap().as_str().parse().unwrap();
        let sy = caps.name("sy").unwrap().as_str().parse().unwrap();
        result.sensors.push(Coord { x: sx, y: sy });

        let bx = caps.name("bx").unwrap().as_str().parse().unwrap();
        let by = caps.name("by").unwrap().as_str().parse().unwrap();
        result.beacons.push(Coord { x: bx, y: by });
    }
    result
}

fn part1(input: &Input) -> u32 {
    let distances: Vec<u32> = input
        .sensors
        .iter()
        .zip(input.beacons.iter())
        .map(|(s, b)| s.dist(b))
        .collect();

    let result_y = 2_000_000;

    let intervals: Vec<(i32, i32)> = input
        .sensors
        .iter()
        .enumerate()
        .map(|(i, s)| {
            // calculate dx from manhattan dist, for points on ressult_y line
            let dx = distances[i] as i32 - s.y.abs_diff(result_y) as i32;
            (dx, s)
        })
        .filter(|(dx, _s)| *dx > 0)
        .map(|(dx, s)| (s.x - dx, s.x + dx))
        .collect();

    let allowed_x: Vec<i32> = input
        .beacons
        .iter()
        .filter(|b| b.y == result_y)
        .map(|b| b.x)
        .collect();

    let min_x = intervals.iter().map(|i| i.0).min().unwrap();
    let max_x = intervals.iter().map(|i| i.1).max().unwrap();

    let mut result = 0;
    for x in min_x..=max_x {
        if allowed_x.contains(&x) {
            continue;
        }

        for (left, right) in intervals.iter() {
            if *left <= x && x <= *right {
                result += 1;
                break;
            }
        }
    }

    result
}

// Line equations:
// y - sy = x - sx + r        A/\B    y - sy = -(x - sx) + r
//                            /  \
//                            \  /
// y - sy = -(x - sx) - r     D\/C    y - sy = x - sx - r
//
// Interesting parts are just constants that depend on the sensor position
// (sx, sy) and its detection radius r.
// i.e. draw a line that goes through sensor, and shift it by r
// the negatibe line has equation: x + y = interesting
// the positive line has equation: x - y = interesting
fn part2(input: &Input) -> u64 {
    let distances: Vec<u32> = input
        .sensors
        .iter()
        .zip(input.beacons.iter())
        .map(|(s, b)| s.dist(b))
        .collect();

    let mut neg_interesting: Vec<i32> = Vec::new();
    let mut pos_interesting: Vec<i32> = Vec::new();
    for (i, s) in input.sensors.iter().enumerate() {
        let r = distances[i] as i32;
        neg_interesting.push(s.x + s.y - r);
        neg_interesting.push(s.x + s.y + r);
        pos_interesting.push(s.x - s.y - r);
        pos_interesting.push(s.x - s.y + r);
    }

    let mut pos_int = 0;
    let mut neg_int = 0;
    let num_sensors = input.sensors.len();
    // 2 times, because every sensor has 2 positive and 2 negative lines
    // that define its reach (see above equations for lines)
    for i in 0..2 * num_sensors {
        for j in i + 1..2 * num_sensors {
            // for every pair of positive lines
            let a = pos_interesting[i];
            let b = pos_interesting[j];
            // if these lines are 2 units apart, final beacon could be there
            // (1 unit from line translated up, and 1 unit from line translated down)
            // we assume there will be only one true condition
            if a.abs_diff(b) == 2 {
                // plus 1 because sensor is in the center
                pos_int = std::cmp::min(a, b) + 1;
            }

            // for every pair of negative lines
            let a = neg_interesting[i];
            let b = neg_interesting[j];
            if a.abs_diff(b) == 2 {
                // plus 1 because sensor is in the center
                neg_int = std::cmp::min(a, b) + 1;
            }
        }
    }

    // intersection between the positive line and negative line that
    // both go through the final point where beacon is located
    // solve these equations for x and y:
    // the negatibe line has equation: x + y = neg_int
    // the positive line has equation: x - y = pos_int
    let intersection_x = (pos_int as u64 + neg_int as u64) / 2;
    let intersection_y = (neg_int as u64 - pos_int as u64) / 2;

    intersection_x * 4_000_000 + intersection_y
}

fn main() {
    let input = parse_input(include_str!("../input/day15.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_zip() {
        let x = [1, 2, 3];
        let y = [4, 5, 6];
        let actual = x
            .iter()
            .zip(y.iter())
            .map(|(a, b)| a + b)
            .collect::<Vec<u8>>();
        let expected = vec![5, 7, 9];
        assert_eq!(expected, actual);
    }
}
