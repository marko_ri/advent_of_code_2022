fn parse_input(input: &str) -> Vec<Vec<i32>> {
    input
        .split("\n\n")
        .map(|elf_cal| {
            elf_cal
                .lines()
                .map(|line| line.parse::<i32>().expect("integer"))
                .collect()
        })
        .collect()
}

fn main() {
    let input = parse_input(include_str!("../input/day01.txt"));
    let mut elf_cal_sum: Vec<i32> = input.iter().map(|elf_cal| elf_cal.iter().sum()).collect();
    elf_cal_sum.sort_unstable_by(|a, b| b.cmp(a));
    let part1 = elf_cal_sum[0];
    let part2: i32 = elf_cal_sum.iter().take(3).sum();
    println!("part1: {:?}", part1);
    println!("part2: {:?}", part2);
}
