use once_cell::sync::Lazy;
use std::collections::{HashMap, HashSet};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Dir {
    Left,
    Right,
}

impl Dir {
    fn from(c: u8) -> Self {
        match c {
            b'<' => Dir::Left,
            b'>' => Dir::Right,
            _ => panic!("invalid input: {:?}", c),
        }
    }
}

static ROCKS: Lazy<Vec<Vec<(usize, usize)>>> = Lazy::new(|| {
    vec![
        vec![(2, 0), (3, 0), (4, 0), (5, 0)],
        vec![(2, 1), (3, 1), (3, 2), (3, 0), (4, 1)],
        vec![(2, 0), (3, 0), (4, 0), (4, 1), (4, 2)],
        vec![(2, 0), (2, 1), (2, 2), (2, 3)],
        vec![(2, 0), (3, 0), (2, 1), (3, 1)],
    ]
});

fn parse_input(input: &str) -> Vec<Dir> {
    input.trim().bytes().map(|b| Dir::from(b)).collect()
}

fn part1(input: &[Dir]) -> usize {
    let rocks_len = ROCKS.len();
    let mut chamber: HashSet<(_, _)> = (0..7).map(|x| (x, 0)).collect();
    let mut highest = 0;
    let mut jet = 0;

    for i in 0..2022 {
        let mut rock = ROCKS[i % rocks_len].clone();
        // add it to y coordinate to get a starting point of a rock
        let adjustment = highest + 4;
        for coord in &mut rock {
            coord.1 += adjustment;
        }

        let mut rest = false;
        while !rest {
            let mut new_rock = vec![];
            if input[jet] == Dir::Left {
                // the first pixel is the leftmost one, and
                // the last is the rightmost one
                if rock[0].0 > 0 {
                    let mut good = true;
                    for coord in &rock {
                        if chamber.contains(&(coord.0 - 1, coord.1)) {
                            good = false;
                            break;
                        }
                        new_rock.push((coord.0 - 1, coord.1));
                    }
                    if good {
                        rock = new_rock;
                    }
                }
            } else {
                // we are moving right
                if rock.last().unwrap().0 < 6 {
                    let mut good = true;
                    for coord in &rock {
                        if chamber.contains(&(coord.0 + 1, coord.1)) {
                            good = false;
                            break;
                        }
                        new_rock.push((coord.0 + 1, coord.1));
                    }
                    if good {
                        rock = new_rock;
                    }
                }
            }

            // move through the jet list, wrapping
            jet = (jet + 1) % input.len();

            // move down if we can
            {
                new_rock = vec![];
                let mut good = true;
                for coord in &rock {
                    if chamber.contains(&(coord.0, coord.1 - 1)) {
                        for c in &rock {
                            chamber.insert((c.0, c.1));
                        }
                        // break out of the 'while' loop
                        rest = true;
                        highest = rock
                            .iter()
                            .map(|c| c.1)
                            .chain([highest].into_iter())
                            .max()
                            .unwrap();
                        good = false;
                        break;
                    } else {
                        new_rock.push((coord.0, coord.1 - 1));
                    }
                }
                if good {
                    rock = new_rock;
                }
            }
        }
    }

    highest
}

fn part2(input: &[Dir]) -> usize {
    let rocks_len = ROCKS.len();
    let mut chamber: HashSet<(_, _)> = (0..7).map(|x| (x, 0)).collect();
    let mut highest = 0;
    let mut jet = 0;

    // keys are 7 y-coordinates, which correspond to the height of
    // the rocks in the chamber, relative to the lowest one of them -
    // see diagram below, plus rock_index and jet_index
    // values is tuple: (highest, total_rocks)
    let mut states: HashMap<Vec<usize>, (usize, usize)> = HashMap::new();
    let mut total_rocks = 0;
    let mut rock_index = 0;
    let mut cycle_found = false;
    let mut height_gain_in_cycle = 0;
    let mut skipped_cycles = 0;

    while total_rocks < 1_000_000_000_000 {
        let mut rock = ROCKS[rock_index].clone();
        // add it to y coordinate to get a starting point of a rock
        let adjustment = highest + 4;
        for coord in &mut rock {
            coord.1 += adjustment;
        }

        let mut rest = false;
        while !rest {
            let mut new_rock = vec![];
            if input[jet] == Dir::Left {
                // the first pixel is the leftmost one, and
                // the last is the rightmost one
                if rock[0].0 > 0 {
                    let mut good = true;
                    for coord in &rock {
                        if chamber.contains(&(coord.0 - 1, coord.1)) {
                            good = false;
                            break;
                        }
                        new_rock.push((coord.0 - 1, coord.1));
                    }
                    if good {
                        rock = new_rock;
                    }
                }
            } else {
                // we are moving right
                if rock.last().unwrap().0 < 6 {
                    let mut good = true;
                    for coord in &rock {
                        if chamber.contains(&(coord.0 + 1, coord.1)) {
                            good = false;
                            break;
                        }
                        new_rock.push((coord.0 + 1, coord.1));
                    }
                    if good {
                        rock = new_rock;
                    }
                }
            }

            // move through the jet list, wrapping
            jet = (jet + 1) % input.len();

            // move down if we can
            {
                new_rock = vec![];
                let mut good = true;
                for coord in &rock {
                    if chamber.contains(&(coord.0, coord.1 - 1)) {
                        for c in &rock {
                            chamber.insert((c.0, c.1));
                        }
                        // break out of the 'while' loop
                        rest = true;
                        highest = rock
                            .iter()
                            .map(|c| c.1)
                            .chain([highest].into_iter())
                            .max()
                            .unwrap();
                        good = false;
                        break;
                    } else {
                        new_rock.push((coord.0, coord.1 - 1));
                    }
                }
                if good {
                    rock = new_rock;
                }
            }
        } // while !rest

        total_rocks += 1;

        // here is where cycle detection happens. we only try it if we haven't
        // already found a cycle earlier
        if !cycle_found {
            // let's say the chamber looks like this:
            //
            //   #     --- y = 5
            //  ###  # --- y = 4
            //  ### ## --- y = 3
            // ####### --- y = 2
            // ####### --- y = 1
            // ####### --- y = 0
            //
            // then after this loop, we'd have:
            // state = vec![2, 4, 5, 4, 2, 3, 4]

            let mut state: Vec<usize> = vec![];
            for i in 0..7 {
                state.push(
                    chamber
                        .iter()
                        .filter(|x| x.0 == i)
                        .map(|x| x.1)
                        .max()
                        .unwrap(),
                )
            }
            let lowest = state.iter().copied().min().unwrap();
            // and now we have:
            // state = vec![0, 2, 3, 2, 0, 1, 2]
            let mut state: Vec<_> = state.into_iter().map(|x| x - lowest).collect();
            // the last two numbers in the key of the states map are
            // positions in the rock array and the jet array
            state.extend([rock_index, jet].into_iter());
            if let Some(state_data) = states.get(&state) {
                // if we've already seen this state we have a loop
                height_gain_in_cycle = highest - state_data.0;
                let rocks_in_cycle = total_rocks - state_data.1;
                skipped_cycles = (1_000_000_000_000 - total_rocks) / rocks_in_cycle;
                total_rocks += skipped_cycles * rocks_in_cycle;
                cycle_found = true;
            } else {
                states.insert(state, (highest, total_rocks));
            }
        }

        rock_index = (rock_index + 1) % rocks_len;
    }

    highest + (skipped_cycles * height_gain_in_cycle)
}

fn main() {
    let input = parse_input(include_str!("../input/day17.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
