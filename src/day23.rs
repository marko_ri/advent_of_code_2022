use std::collections::{HashMap, HashSet};

#[derive(Clone, PartialEq, Eq, Hash)]
struct Coord {
    row: i32,
    col: i32,
}

impl Coord {
    fn add(&self, other: &Self) -> Self {
        Self {
            row: self.row + other.row,
            col: self.col + other.col,
        }
    }
}

#[derive(Clone)]
struct Input {
    elves: HashSet<Coord>,
    dirs: [Coord; 8],
    check_dirs: [([usize; 3], usize); 4],
}

impl Input {
    fn from(input: &str) -> Self {
        let mut elves = HashSet::new();
        for (row, line) in input.trim().lines().enumerate() {
            for (col, cell) in line.bytes().enumerate() {
                if cell == b'#' {
                    elves.insert(Coord {
                        row: row as i32,
                        col: col as i32,
                    });
                }
            }
        }

        let dirs = [
            // E
            Coord { row: 0, col: 1 },
            // NE
            Coord { row: -1, col: 1 },
            // N
            Coord { row: -1, col: 0 },
            // NW
            Coord { row: -1, col: -1 },
            // W
            Coord { row: 0, col: -1 },
            // SW
            Coord { row: 1, col: -1 },
            // S
            Coord { row: 1, col: 0 },
            // SE
            Coord { row: 1, col: 1 },
        ];

        let check_dirs = [
            // NE, N, NW -> N
            ([1, 2, 3], 2),
            // SE, S, SW  -> S
            ([7, 6, 5], 6),
            // SW, W, NW -> W
            ([5, 4, 3], 4),
            // NE, E, SE -> E
            ([1, 0, 7], 0),
        ];

        Self {
            elves,
            dirs,
            check_dirs,
        }
    }
}

struct Bounds {
    min_row: i32,
    max_row: i32,
    min_col: i32,
    max_col: i32,
}

impl Bounds {
    fn new(input: &HashSet<Coord>) -> Self {
        let mut min_row = std::i32::MAX;
        let mut max_row = std::i32::MIN;
        let mut min_col = std::i32::MAX;
        let mut max_col = std::i32::MIN;

        for &Coord { row, col } in input.iter() {
            min_row = std::cmp::min(min_row, row);
            max_row = std::cmp::max(max_row, row);

            min_col = std::cmp::min(min_col, col);
            max_col = std::cmp::max(max_col, col);
        }

        Self {
            min_row,
            max_row,
            min_col,
            max_col,
        }
    }
}

fn parse_input(input: &str) -> Input {
    Input::from(input)
}

fn run(input: &Input) -> Option<HashSet<Coord>> {
    // stage 1: make proposals
    let mut propose_map: HashMap<Coord, Coord> = HashMap::new();
    let mut proposed_map: HashMap<Coord, u32> = HashMap::new();
    for elf in input.elves.iter() {
        // if elf has no neighbors, continue
        let mut good = false;
        for direction in input.dirs.iter() {
            if input.elves.contains(&elf.add(direction)) {
                good = true;
                break;
            }
        }
        if !good {
            continue;
        }

        for (directions, propose_dir) in input.check_dirs.iter() {
            good = true;
            for direction in directions.iter() {
                let d = &input.dirs[*direction];
                if input.elves.contains(&elf.add(d)) {
                    good = false;
                    break;
                }
            }
            if !good {
                continue;
            }

            let prop = &input.dirs[*propose_dir];
            propose_map.insert(elf.clone(), elf.add(prop));
            proposed_map
                .entry(propose_map.get(elf).unwrap().clone())
                .and_modify(|v| *v += 1)
                .or_insert(1);
            break;
        }
    }

    if propose_map.is_empty() {
        return None;
    }

    // Stage 2: do the poposals
    let mut new_elves: HashSet<Coord> = HashSet::new();
    for elf in input.elves.iter() {
        if let Some(new_loc) = propose_map.get(elf) {
            if *proposed_map.get(new_loc).unwrap_or(&0) > 1 {
                // keep the old position
                new_elves.insert(elf.clone());
            } else {
                new_elves.insert(new_loc.clone());
            }
        } else {
            // keep the old position
            new_elves.insert(elf.clone());
        }
    }

    Some(new_elves)
}

fn part1(mut input: Input) -> i32 {
    for _ in 0..10 {
        if let Some(new_elves) = run(&input) {
            input.elves = new_elves;
            // at the end of the round, the first direction the Elves considered
            // is moved to the end of the list of directions
            input.check_dirs.rotate_left(1);
        } else {
            unreachable!()
        }
    }

    let bounds = Bounds::new(&input.elves);
    (bounds.max_row - bounds.min_row + 1) * (bounds.max_col - bounds.min_col + 1)
        - input.elves.len() as i32
}

fn part2(mut input: Input) -> i32 {
    let mut round = 1;
    loop {
        if let Some(new_elves) = run(&input) {
            input.elves = new_elves;
            // at the end of the round, the first direction the Elves considered
            // is moved to the end of the list of directions
            input.check_dirs.rotate_left(1);
        } else {
            break;
        }
        round += 1;
    }
    round
}

fn main() {
    let input = parse_input(include_str!("../input/day23.txt"));
    let part1 = part1(input.clone());
    println!("part1: {:?}", part1);
    let part2 = part2(input);
    println!("part2: {:?}", part2);
}
