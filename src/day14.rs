use std::collections::HashSet;

const SAND_SOURCE: Coord = Coord { x: 500, y: 0 };

#[derive(PartialEq, Eq, Hash, Debug, Clone, Default)]
struct Coord {
    x: u16,
    y: u16,
}

fn parse_input(input: &str) -> HashSet<Coord> {
    let mut filled = HashSet::new();

    for line in input.lines() {
        let mut coords: Vec<Coord> = Vec::new();
        for str_coord in line.split(" -> ") {
            let (x, y) = str_coord.split_once(',').unwrap();
            coords.push(Coord {
                x: x.parse().unwrap(),
                y: y.parse().unwrap(),
            });
        }

        for window in coords.windows(2) {
            let prev = &window[0];
            let curr = &window[1];

            if curr.y != prev.y {
                debug_assert!(curr.x == prev.x);
                for y in std::cmp::min(curr.y, prev.y)..=std::cmp::max(curr.y, prev.y) {
                    filled.insert(Coord { x: curr.x, y });
                }
            }

            if curr.x != prev.x {
                debug_assert!(curr.y == prev.y);
                for x in std::cmp::min(curr.x, prev.x)..=std::cmp::max(curr.x, prev.x) {
                    filled.insert(Coord { x, y: curr.y });
                }
            }
        }
    }

    filled
}

fn simulate_sand<F>(filled: &mut HashSet<Coord>, loop_cond: F) -> Coord
where
    F: Fn(u16) -> bool,
{
    let mut sand = SAND_SOURCE;

    while loop_cond(sand.y) {
        if !filled.contains(&Coord {
            x: sand.x,
            y: sand.y + 1,
        }) {
            sand.y += 1;
            continue;
        }

        if !filled.contains(&Coord {
            x: sand.x - 1,
            y: sand.y + 1,
        }) {
            sand.x -= 1;
            sand.y += 1;
            continue;
        }

        if !filled.contains(&Coord {
            x: sand.x + 1,
            y: sand.y + 1,
        }) {
            sand.x += 1;
            sand.y += 1;
            continue;
        }

        // everything is filled, come to rest
        break;
    }

    sand
}

fn part1(mut input: HashSet<Coord>) -> u16 {
    let max_y = input.iter().map(|c| c.y).max().unwrap();
    let mut result = 0;
    let loop_cond = |y| y < max_y;
    loop {
        let sand = simulate_sand(&mut input, loop_cond);
        let should_stop = sand.y >= max_y;
        if should_stop {
            break;
        }
        // put it in set if it is less than max_y
        input.insert(sand);
        result += 1;
    }
    result
}

fn part2(mut input: HashSet<Coord>) -> u16 {
    let max_y = input.iter().map(|c| c.y).max().unwrap();
    let mut result = 0;
    let loop_cond = |y| y <= max_y;
    loop {
        let sand = simulate_sand(&mut input, loop_cond);
        let should_stop = sand == SAND_SOURCE;
        // put it in set if it is less, equal OR greater by one than max_y
        input.insert(sand);
        result += 1;
        if should_stop {
            break;
        }
    }
    result
}

fn main() {
    let input = parse_input(include_str!("../input/day14.txt"));
    let part1 = part1(input.clone());
    println!("part1: {:?}", part1);
    let part2 = part2(input);
    println!("part2: {:?}", part2);
}
