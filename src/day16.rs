use std::collections::HashMap;

/*
Convert the valves into a table (weighted graph) and generate a min distance
matrix by using floyd warshall.

How we keep track of open valves:
Let's say that we have a list of 8 valves, and the starting valve is at index 3
The starting bitmask in this case would look like this: 0b11110111
Open valves are represented by 0s and closed ones are 1s

The size of the bitmask is based on how many valves we want to keep track of
in our case the size is 2^8 - 1
2^8     = 0b100000000
2^8 - 1 = 0b011111111

As we perform the simulation we modify the bitmask according to what valves are
opened, and we keep track of the highest possible flow rate out of each path
combination. We also use a hashmap that keeps track the highest flow rate that
we can get out of different valve combinations (This will be important for part2)

# Part 1

This part is pretty straightforward, we can just use the highest flow rate
returned by the simulation

# Part 2

Here, we simulate the paths of the elf and the elpehant separately which results
in 2 path hashmaps. After that we loop over each of the elf's paths and find paths
that the elephant took that don't overlap (using some bitmasks magic) with the elf
path being checked; Overlap here means that both of them opened the same valves,
so we basically discard all paths where both of them have at least 1 valve in
common because a valve can only be opened once.

(! elf_mask) & (! elephant_mask) & init_mask == 0
The reason why we flip elf_mask's and elephant_mask's bits is because open
valves are represented by 0s instead of 1s and then we have to AND the result
with the initial mask because when we flip the bits of a number, all of the bits
are flipped (depending on the integer size being used) and not only the ones
being used, for example if we have:

0b0011 that is stored within a u16 and we flip the bits, we'll end up with
0b1111111111111100,
as you can see the flipping will happen across all 16 bits, and that's why
we AND the result with the initial_mask.

If the result of the above bitmask expression results in a number that's bigger
than 0 that means that we have overlaps and we have to skip the current pair.
For pairs that don't have overlaps we add their highest flow rates together
and compare them with the rest.
*/

#[derive(Debug)]
struct Valve {
    name: String,
    rate: u32,
    links: Vec<usize>,
}

impl Valve {
    fn from(input: &str, names: &[String]) -> Self {
        let mut iter = input.splitn(10, ' ');
        let name = iter.nth(1).unwrap().to_owned();
        let rate = iter
            .nth(2)
            .unwrap()
            .split('=')
            .last()
            .unwrap()
            .strip_suffix(';')
            .unwrap()
            .parse()
            .unwrap();
        let links: Vec<usize> = iter
            .last()
            .unwrap()
            .split(", ")
            .map(|link| names.iter().position(|n| n == link).unwrap())
            .collect();

        Valve { name, rate, links }
    }
}

fn parse_input(input: &str) -> Vec<Valve> {
    // first pass - collect names (we need to know valve indexes)
    let names: Vec<String> = input
        .lines()
        .map(|line| line.split_ascii_whitespace().nth(1).unwrap().to_owned())
        .collect();

    input
        .lines()
        .map(|line| Valve::from(line, &names))
        .collect()
}

fn floyd_warshall(graph: &[Vec<u32>]) -> Vec<Vec<u32>> {
    let len = graph.len();
    let mut result = graph.to_vec();

    for k in 0..len {
        for i in 0..len {
            for j in 0..len {
                if result[i][k] + result[k][j] < result[i][j] {
                    result[i][j] = result[i][k] + result[k][j];
                }
            }
        }
    }

    result
}

fn init_graph(input: &[Valve]) -> Vec<Vec<u32>> {
    let len = input.len();
    let mut result = vec![vec![u32::MAX / 4; len]; len];

    for (i, valve) in input.iter().enumerate() {
        for link in valve.links.iter() {
            result[i][*link] = 1;
        }
    }

    result
}

fn simulate(
    input: &[Valve],
    dist: &[Vec<u32>],
    init_mask: u64,
    start_idx: usize,
    minutes: u32,
) -> (u32, HashMap<u64, u32>) {
    let non_zero_valves: Vec<usize> = input
        .iter()
        .enumerate()
        .filter(|(_i, v)| v.rate > 0)
        .map(|(i, _v)| i)
        .collect();

    // keys: bitmask, values: highest flow rate
    let mut mask_flow: HashMap<u64, u32> = HashMap::new();
    let flow = recursive(
        input,
        &mut mask_flow,
        &non_zero_valves,
        dist,
        init_mask,
        minutes,
        0,
        start_idx,
    );

    (flow, mask_flow)
}

fn recursive(
    input: &[Valve],
    memo: &mut HashMap<u64, u32>,
    non_zero_valves: &[usize],
    dist: &[Vec<u32>],
    mask: u64,
    minutes: u32,
    flow: u32,
    idx: usize,
) -> u32 {
    let mut max_flow = flow;

    memo.insert(mask, *memo.get(&mask).unwrap_or(&0).max(&flow));

    for &j in non_zero_valves.iter() {
        let cur_minutes = minutes
            .checked_sub(dist[idx][j])
            .and_then(|x| x.checked_sub(1))
            .unwrap_or(0);

        // if this valve was already opened, skip it
        if (mask & (1 << j)) == 0 || cur_minutes <= 0 {
            continue;
        }

        let cur_mask = mask & !(1 << j);

        let cur_flow = flow + (cur_minutes * input[j].rate);

        max_flow = max_flow.max(recursive(
            input,
            memo,
            non_zero_valves,
            dist,
            cur_mask,
            cur_minutes,
            cur_flow,
            j,
        ));
    }

    max_flow
}

fn part1(input: &[Valve]) -> u32 {
    let graph = init_graph(input);
    let dist = floyd_warshall(&graph);

    let init_mask: u64 = (1 << dist.len()) - 1;
    let start_idx = input.iter().position(|v| &v.name == "AA").unwrap();

    let (flow, _) = simulate(input, &dist, init_mask, start_idx, 30);

    flow
}

fn part2(input: &[Valve]) -> u32 {
    let graph = init_graph(input);
    let dist = floyd_warshall(&graph);

    let init_mask: u64 = (1 << dist.len()) - 1;
    let start_idx = input.iter().position(|v| &v.name == "AA").unwrap();

    let (_, elf_memo) = simulate(input, &dist, init_mask, start_idx, 26);
    let (_, elephant_memo) = simulate(input, &dist, init_mask, start_idx, 26);

    let mut max_flow = 0;
    for (elf_mask, elf_flow) in elf_memo.iter() {
        for (elephant_mask, elephant_flow) in elephant_memo.iter() {
            // check that there is no ovelap between two paths
            if (!elephant_mask) & (!elf_mask) & init_mask == 0 {
                max_flow = std::cmp::max(max_flow, elephant_flow + elf_flow);
            }
        }
    }

    max_flow
}

fn main() {
    let input = parse_input(include_str!("../input/day16.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
