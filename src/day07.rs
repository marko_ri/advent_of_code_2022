use std::collections::HashMap;
use std::path::PathBuf;

// for given example:
// children: {"/": ["/a", "/d"], "/a": ["/a/e"]},
// sizes: {"/": 23352670, "/a": 94269, "/d": 24933642, "/a/e": 584} }
#[derive(Debug)]
struct Input {
    children: HashMap<PathBuf, Vec<PathBuf>>,
    sizes: HashMap<PathBuf, u32>,
}

fn parse_input(input: &str) -> Input {
    let mut path = PathBuf::from("/");
    let mut children: HashMap<PathBuf, Vec<PathBuf>> = HashMap::new();
    let mut sizes: HashMap<PathBuf, u32> = HashMap::new();
    let mut size = 0;
    let mut ls = false;

    for line in input.lines() {
        let parts: Vec<_> = line.split_whitespace().collect();
        match parts[..] {
            ["$", "cd", ".."] => {
                if ls {
                    sizes.insert(PathBuf::from(&path), size);
                    size = 0;
                    ls = false;
                }
                path.pop();
            }
            ["$", "cd", name] => {
                if ls {
                    sizes.insert(PathBuf::from(&path), size);
                    size = 0;
                    ls = false;
                }
                path.push(name);
            }
            ["$", "ls"] => {
                ls = true;
            }
            ["dir", name] => {
                let mut new_path = PathBuf::from(&path);
                new_path.push(name);
                match children.get_mut(&path) {
                    Some(child) => {
                        child.push(new_path);
                    }
                    None => {
                        children.insert(path.clone(), vec![new_path]);
                    }
                }
            }
            [file_size, _name] => {
                size += file_size.parse::<u32>().expect("file size");
            }
            _ => panic!("unexpected input"),
        }
    }

    // include the last "ls" command
    if ls {
        sizes.insert(PathBuf::from(&path), size);
    }

    Input { children, sizes }
}

fn total_size(path: &PathBuf, input: &Input) -> u32 {
    let mut size = *input.sizes.get(path).unwrap();
    if let Some(children) = input.children.get(path) {
        for child in children {
            size += total_size(child, input);
        }
    }

    size
}

fn part1(input: &Input) -> u32 {
    let mut result = 0;
    for path in input.sizes.keys() {
        let size = total_size(path, input);
        if size <= 100_000 {
            result += size;
        }
    }
    result
}

fn part2(input: &Input) -> u32 {
    let unused = 70_000_000 - total_size(&PathBuf::from("/"), input);
    let required = 30_000_000 - unused;

    let mut result = std::u32::MAX;
    for path in input.sizes.keys() {
        let size = total_size(path, input);
        if size >= required {
            result = std::cmp::min(result, size);
        }
    }

    result
}

fn main() {
    let input = parse_input(include_str!("../input/day07.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(&input);
    println!("part2: {:?}", part2);
}
