use std::cmp::Ordering;
use std::iter::Peekable;
use std::str::Chars;

#[derive(Clone, Eq)]
enum Packet {
    Many(Vec<Packet>),
    Single(i32),
}

impl PartialEq for Packet {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Packet {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Packet::Single(x), Packet::Single(y)) => x.cmp(y),
            (Packet::Many(x), Packet::Many(y)) => x.cmp(y),
            (x @ Packet::Single(_), Packet::Many(y)) => std::slice::from_ref(x).cmp(y.as_slice()),
            (Packet::Many(x), y @ Packet::Single(_)) => x.as_slice().cmp(std::slice::from_ref(y)),
        }
    }
}

#[derive(PartialEq, Eq, Debug)]
enum Token {
    Open,
    Close,
    Comma,
    Integer(i32),
}

struct Tokens<'a> {
    stream: Peekable<Chars<'a>>,
}

impl<'a> Tokens<'a> {
    fn new(s: &'a str) -> Tokens<'a> {
        Tokens {
            stream: s.chars().peekable(),
        }
    }
}

impl<'a> Iterator for Tokens<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match *self.stream.peek()? {
                '[' => {
                    self.stream.next();
                    return Some(Token::Open);
                }
                ']' => {
                    self.stream.next();
                    return Some(Token::Close);
                }
                ',' => {
                    self.stream.next();
                    return Some(Token::Comma);
                }
                c => {
                    if c.is_whitespace() {
                        self.stream.next();
                        continue;
                    }

                    assert!(c.is_digit(10));

                    let mut digits = String::from("");
                    while let Some(d) = self.stream.next_if(|&c| c.is_digit(10)) {
                        digits.push(d);
                    }

                    return Some(Token::Integer(digits.parse().expect("integer")));
                }
            }
        }
    }
}

#[test]
fn test_tokenizing() {
    assert_eq!(
        Tokens::new("[[8,[1,9],6]]").into_iter().collect::<Vec<_>>(),
        [
            Token::Open,
            Token::Open,
            Token::Integer(8),
            Token::Comma,
            Token::Open,
            Token::Integer(1),
            Token::Comma,
            Token::Integer(9),
            Token::Close,
            Token::Comma,
            Token::Integer(6),
            Token::Close,
            Token::Close
        ]
    );
}

fn parse_many(tokens: &mut Tokens) -> Vec<Packet> {
    let mut result = vec![];

    let mut tok = tokens.next().expect("start of many element");
    // base condition
    if let Token::Close = tok {
        return result;
    }

    loop {
        match tok {
            Token::Integer(i) => result.push(Packet::Single(i)),
            Token::Open => result.push(Packet::Many(parse_many(tokens))),
            _ => panic!("unexpected token: {:?}", tok),
        }

        match tokens.next().expect("after element") {
            Token::Close => break,
            Token::Comma => {
                // skip comma, and take next elem
                tok = tokens.next().expect("next element");
                continue;
            }
            tok => panic!("unexpected token: {:?}", tok),
        }
    }

    result
}

fn parse_packet(s: &str) -> Packet {
    let mut tokens = Tokens::new(s);

    if let Token::Open = tokens.next().expect("token") {
        return Packet::Many(parse_many(&mut tokens));
    } else {
        panic!("invalid input");
    }
}

fn parse_input(input: &str) -> Vec<Packet> {
    input
        .lines()
        .filter(|s| !s.is_empty())
        .map(parse_packet)
        .collect()
}

fn part1(input: &[Packet]) -> usize {
    input
        .chunks(2)
        .enumerate()
        .filter_map(
            |(idx, chunk)| match chunk[0].cmp(&chunk[1]) == Ordering::Less {
                true => Some(idx + 1),
                false => None,
            },
        )
        .sum()
}

fn part2(mut input: Vec<Packet>) -> usize {
    input.sort_unstable();
    // We can push the two extra packets and then binary search for them,
    // or we can just binary search for them and take the `Err` variant,
    // because it points to where they should have been if they were present,
    // without having to add them to the vector
    //
    // We can also simplify the packets. Instead of `[[x]]` We can just push `[x]`

    // Add +1 because Rust's indexes are 0-based, while this task uses 1-based indexing
    let a = input.binary_search(&Packet::Single(2)).unwrap_err() + 1;
    // Add +1 because Rust's indexes are 0-based, while this task uses 1-based indexing
    // Add one more +1 to compensate for the missing `2`, thus for a total of `+2`
    let b = input.binary_search(&Packet::Single(6)).unwrap_err() + 2;

    a * b
}

fn main() {
    let input = parse_input(include_str!("../input/day13.txt"));
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
    let part2 = part2(input);
    println!("part2: {:?}", part2);
}
