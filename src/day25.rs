use std::collections::HashMap;

const BASE: i64 = 5;

fn snafu_int(line: &str, char_dig: &HashMap<u8, i64>) -> i64 {
    let mut result = 0;
    let mut p = 1;
    for i in (0..line.len()).rev() {
        result += p * char_dig.get(&line.as_bytes()[i]).unwrap();
        p *= BASE;
    }
    result
}

fn int_snafu(n: i64, dig_char: &HashMap<i64, u8>) -> String {
    let mut result = String::new();
    let mut n = n;
    while n > 0 {
        let d = ((n + 2) % BASE) - 2;
        result.push(char::from(*dig_char.get(&d).unwrap()));
        n -= d;
        n /= BASE;
    }
    result.chars().rev().collect()
}

fn part1(input: &str) -> String {
    let mut char_dig = HashMap::new();
    char_dig.insert(b'0', 0);
    char_dig.insert(b'1', 1);
    char_dig.insert(b'2', 2);
    char_dig.insert(b'-', -1);
    char_dig.insert(b'=', -2);

    let mut dig_char = HashMap::new();
    for (k, v) in char_dig.iter() {
        dig_char.insert(*v, *k);
    }

    let result = input.lines().map(|line| snafu_int(line, &char_dig)).sum();
    int_snafu(result, &dig_char)
}

fn main() {
    let input = include_str!("../input/day25.txt");
    let part1 = part1(&input);
    println!("part1: {:?}", part1);
}
