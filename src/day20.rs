fn parse_input(input: &str, key: isize) -> Vec<(usize, isize)> {
    input
        .lines()
        .map(|l| l.parse::<isize>().unwrap() * key)
        .enumerate()
        .collect()
}

fn run(input: &str, key: isize, rounds: u8) -> isize {
    let input = parse_input(input, key);
    let input_len = input.len();
    let mut numbers = input.clone();

    for _ in 0..rounds {
        for (orig_ind, orig_num) in input.iter() {
            let ind = numbers.iter().position(|(i, _x)| i == orig_ind).unwrap();
            let new_ind = (ind as isize + orig_num).rem_euclid(input_len as isize - 1);
            let tmp = numbers.remove(ind);
            numbers.insert(new_ind as usize, tmp);
        }
    }

    let zero_ind = numbers.iter().position(|x| x.1 == 0).unwrap();
    let mut result = numbers[(zero_ind + 1000) % input_len].1;
    result += numbers[(zero_ind + 2000) % input_len].1;
    result += numbers[(zero_ind + 3000) % input_len].1;

    result
}

fn main() {
    let input = include_str!("../input/day20.txt");
    let part1 = run(&input, 1, 1);
    println!("part1: {:?}", part1);
    let part2 = run(&input, 811_589_153, 10);
    println!("part2: {:?}", part2);
}
